mapboxgl.accessToken = 'pk.eyJ1IjoiaGFybWVubGljaHQiLCJhIjoiY2szNHUzNHNxMGx5ZDNvcGpxdXBxanB4eiJ9.uOFbcdL1X2NPizjjHk5NbQ';
const map = new mapboxgl.Map({
    container: 'map',
    style: 'mapbox://styles/mapbox/streets-v11',
    center: [-74.50, 40],
    zoom: 9
});

const nav = new mapboxgl.NavigationControl();
map.addControl(nav, 'top-right');

let poiList = {};
loadFromLocalStorage();

function addMarker(lng, lat){
    new mapboxgl.Marker()
    .setLngLat([lng, lat])
    .addTo(map);

    let rid = `pow${Math.random().toString().replace('0.', '')}`;

    poiList[rid] = {lng, lat}

    let node = document.createElement("SPAN");
    let POI = document.createTextNode(`${lng}, ${lat}`)
    node.appendChild(POI);
    node.setAttribute('id', rid)
    node.setAttribute('onmousedown', `poiAction()`)
    document.querySelector('.POI').appendChild(node);
}

map.on('click', function (e) {
    if(e.originalEvent.altKey){
        let lngLat = e.lngLat
        addMarker(lngLat.lng, lngLat.lat)
        localStorage.setItem('POIList', JSON.stringify(poiList));
    }
});  

function poiAction(){
    event.preventDefault();
    switch(event.which){
        case 1:
            moveToPoi(event.srcElement.id);
            break;
        case 2:
            deletePoi(event.srcElement.id);
    }
}

function deletePoi(id){
    delete poiList[id];
    localStorage.setItem('POIList', JSON.stringify(poiList));
    document.getElementById(id).remove();
}

function moveToPoi(id){
    let POI = poiList[id];
    map.flyTo({
        center: [POI.lng, POI.lat]
        });
}

function loadFromLocalStorage(){
    let localObj = JSON.parse(localStorage.getItem('POIList'));
    if(localObj){
        for(item in localObj){
            addMarker(localObj[item].lng, localObj[item].lat);
        }
    }
}

